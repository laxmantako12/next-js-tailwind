'use client';

import { useState } from 'react';
import { Dialog } from '@headlessui/react';
import {Home, User } from "lucide-react";
// import { useRouter } from "next/router";

import {Menu, X } from "lucide-react";
import Image from 'next/image';
import { usePathname } from 'next/navigation'
import Link from 'next/link';
import MainLogo from "../../public/images/main-logo2.svg";
import MainLogoG from "../../public/images/gray-logo.svg";
import LangSwitch from './LangSwitch'
import LanguageBar from './LanguageBar'


const navigation = [
  {
    name: "Home",
    href: "/",
    icon: <Home className="inline-flex mr-1" size={15} />,
  },
  { name: "About", href: "/about", icon: "" },
  { name: "Events", href: "/events", icon: "" },
  { name: "Solutions", href: "/solutions", icon: "" },
  { name: "Resources", href: "/resources", icon: "" },
  { name: "Contact", href: "/contact", icon: "" },
  {
    name: "Portals",
    href: "/portals",
    icon: <User className="inline-flex mr-1" size={15} />,
  },
];
const mainNav = () => {
 // const router = useRouter();
 const pathname = usePathname()
 const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
 return (
   <header className="inset-x-0 top-0 z-50 topnav bg-white shadow">
     <div className="container">
       <nav
         className="flex items-center justify-between pt-2 pb-6 lg:pt-5 lg:pb-5"
         aria-label="Global"
       >
         <div className="flex lg:flex-1">
           <Link href="/participant" className="-m-1.5 p-1.5">
             <span className="sr-only">Your Company</span>
             <Image
               className=" w-auto"
               src={MainLogo}
               alt="Picture of the author"
             />
           </Link>
         </div>
         <div className="right">
           <div className="flex lg:hidden">
             <button
               type="button"
               className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
               onClick={() => setMobileMenuOpen(true)}
             >
               <span className="sr-only">Open main menu</span>
               {/* <AlignJustify strokeWidth={1} /> */}
               <Menu strokeWidth={1} color="white" />
             </button>
           </div>
           <div className="hidden lg:flex lg:gap-x-12">
             {navigation.map((item) => (
               <Link
                 key={item.name}
                 href={item.href}
                 // className={`text-[11px] tracking-[0.15rem] uppercase font-bold text-primary hover:text-secondary transition`}
                 className={`link ${
                   pathname === item.href ? "active" : ""
                 } text-[11px] tracking-[0.15rem] uppercase font-normal text-primary hover:text-secondary transition [&.active]:text-secondary`}
               >
                 {item.icon}
                 {item.name}
               </Link>
             ))}
           </div>
           {/* <LangSwitch /> */}
           <LanguageBar />
         </div>
       </nav>
     </div>
     <Dialog
       className="lg:hidden"
       open={mobileMenuOpen}
       onClose={setMobileMenuOpen}
     >
       <div className="fixed inset-0 z-50" />
       <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
         <div className="flex items-center justify-between">
           <Link href="/participant" className="-m-1.5 p-1.5">
             <span className="sr-only">Your Company</span>
             <Image
               className="h-8 w-auto"
               src={MainLogoG}
               alt="Picture of the logo"
             />
           </Link>
           <button
             type="button"
             className="-m-2.5 rounded-md p-2.5 text-gray-700"
             onClick={() => setMobileMenuOpen(false)}
           >
             <span className="sr-only">Close menu</span>
             {/* <XMarkIcon className="h-6 w-6" aria-hidden="true" /> */}
             <X strokeWidth={1} />
           </button>
         </div>
         <div className="mt-6 flow-root">
           <div className="-my-6 divide-y divide-gray-500/10">
             <div className="space-y-2 py-6">
               {navigation.map((item) => (
                 <Link
                   key={item.name}
                   href={item.href}
                   className={`link ${
                     pathname === item.href ? "active" : ""
                   } text-[11px] tracking-[0.15rem] uppercase font-bold text-primary hover:text-secondary transition [&.active]:text-secondary`}
                 >
                   {item.name}
                 </Link>
               ))}
             </div>
           </div>
         </div>
       </Dialog.Panel>
     </Dialog>
   </header>
 );
}

export default mainNav
