"use client";
import { Fragment, useState } from "react";
import { Menu, Transition } from "@headlessui/react";

import { useTranslation } from "react-i18next";

import { COMMON } from "../constants/translations";
import { ChevronDown } from "lucide-react";
// import { Button } from "@/components/ui/button";
import {
  humanReadableLanguage,
  supportedLanguages,
} from "../constants/languages";

function LanguageSelectorDropdown() {
  const { i18n } = useTranslation();

  const handleLanguageChange = (nextLocale: string) => {
    i18n.changeLanguage(nextLocale);
  };

  return (
    <Menu as="div" className="relative inline-block text-start">
      <div>
        <Menu.Button className="inline-flex w-full justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-electric-green focus:ring-offset-2 focus:ring-offset-gray-100">
          {humanReadableLanguage(i18n.language)}
          <ChevronDown
          />
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 end-0 focus:outline-none">
          <div className="py-1">
            {supportedLanguages.map((language) => (
              <Menu.Item key={language}>
                {({ active }) => (
                  <>
                    <button
                      onClick={() => handleLanguageChange(language)}
                      className={clsx(
                        active ? "bg-gray-100 text-gray-900" : "text-gray-700",
                        "block  w-full px-4 py-2 text-sm"
                      )}
                    >
                      {humanReadableLanguage(language)}
                    </button>
                    <LanguageSelectorDropdown />
                  </>
                )}
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
}
const LangSwitch = () => {
  const { t } = useTranslation(COMMON);
  return <div>LangSwitch</div>;
};

export default LangSwitch;
