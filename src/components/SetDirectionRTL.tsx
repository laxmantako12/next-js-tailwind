"use client";

import React from "react";

function SetDirectionRTL() {
  const handleClick = () => {
    document.documentElement.setAttribute("dir", "rtl");
  };

  return <button onClick={handleClick}>Set Direction to RTL</button>;
}

export default SetDirectionRTL;
