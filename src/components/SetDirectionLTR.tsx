"use client";

import React from "react";

function SetDirectionLTR() {
  const handleClick = () => {
    document.documentElement.setAttribute("dir", "ltr");
  };

  return <button onClick={handleClick}>Set Direction to LTR</button>;
}

export default SetDirectionLTR;
