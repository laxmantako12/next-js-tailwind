import React from 'react'
import TextBlock from "./components/TextBlock";
import SearchTable from './components/SearchTable'
const Search = () => {
  return (
    <>
      <TextBlock />
      <SearchTable />
    </>
  );
}

export default Search
