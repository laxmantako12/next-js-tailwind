import React from 'react'

const TextBlock = () => {
  return (
    <div className="container my-10 p-6 shadow">
      <p className="text-normal text-primary/80 mb-6">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas facere
        neque iste facilis cupiditate, ex natus eum rerum molestiae dicta nisi
        adipisci deserunt! Provident et hic ut iure assumenda magni!
      </p>

      <p className="text-normal text-primary/80 mb-6">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas facere
        neque iste facilis cupiditate, ex natus eum rerum molestiae dicta nisi
        adipisci deserunt! Provident et hic ut iure assumenda magni!
      </p>

      <p className="text-normal text-primary/80 mb-0">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas facere
        neque iste facilis cupiditate, ex natus eum rerum molestiae dicta nisi
        adipisci deserunt! Provident et hic ut iure assumenda magni!
      </p>
    </div>
  );
}

export default TextBlock
