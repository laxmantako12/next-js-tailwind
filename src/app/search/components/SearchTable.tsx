"use client";
import React, { useState } from "react";
import * as ReactDOM from "react-dom";
import { Input } from "@/components/ui/input";

import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { datatableUsers } from "./data";

const SearchTable = () => {
  const [searchInput, SetSearchInput] = useState("");

  const FilteredData = () => {
    return datatableUsers.filter(
      (user) =>
        user.name.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.position.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.gender.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.office.toLowerCase().includes(searchInput.toLowerCase()) ||
        user.email.toLowerCase().includes(searchInput.toLowerCase())
    );
  };
  return (
    <>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10">
            <div className="card">
              <div className="pt-6 mb-5">
                <div className="flex flex-wrap justify-between">
                  <div className="basis-1/2">
                    {FilteredData().length === datatableUsers.length ? (
                      ""
                    ) : (
                      <h5 className="text-primary">
                        Search ({FilteredData().length}) result found from{" "}
                        {datatableUsers.length}
                      </h5>
                    )}
                  </div>
                  <div className="basis-1/3">
                    <div className="form-group mb-0">
                      <Input
                        type="text"
                        className="form-control"
                        placeholder="Search"
                        value={searchInput}
                        onChange={(e) => SetSearchInput(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <Table className="table-auto border shadow">
                {/* <TableCaption>A list of your recent invoices.</TableCaption> */}
                <TableHeader>
                  <TableRow>
                    <TableHead className="w-[100px] text-primary">#</TableHead>
                    <TableHead className="text-primary">Name</TableHead>
                    <TableHead className="text-primary">Position</TableHead>
                    <TableHead className="text-primary">Gender</TableHead>
                    <TableHead className="text-primary">Office</TableHead>
                    <TableHead className="text-primary">Phone</TableHead>
                    <TableHead className="text-primary">Salary</TableHead>
                    <TableHead className="text-right text-primary">
                      Email
                    </TableHead>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {FilteredData().map((data, index) => {
                    const {
                      id,
                      name,
                      position,
                      gender,
                      office,
                      phone,
                      salary,
                      email,
                    } = data;
                    return (
                      <TableRow key={index}>
                        <TableCell className="font-light text-primary/80">
                          {id}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {name}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {position}{" "}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {gender}{" "}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {office}{" "}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {phone}{" "}
                        </TableCell>
                        <TableCell className="font-light text-primary/80">
                          {salary}{" "}
                        </TableCell>
                        <TableCell className="text-right font-light text-primary/80">
                          {email}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchTable;
