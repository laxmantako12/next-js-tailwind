import React from 'react'
import Banner from './components/Banner'
import Search from './components/Search'
import List from './components/List'

const landingPage = () => {
  return (
    <>
      <Banner />
      <Search />
      <List />
    </>
  );
}

export default landingPage
