import React from 'react'
import Image from 'next/image';
import bannerImage from '../../../../public/images/8_WomeninLeadership_1200x675-1.png'

const Banner = () => {
  return (
    <div className="banner">
      <div className="container">
        <div className="lg:flex flex-wrap ">
          <div className="lg:basis-1/2 bg-primary">
            <div className="bg-primary p-10"></div>
          </div>
          <div className="lg:basis-1/2">
            <Image src={bannerImage} priority />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Banner
