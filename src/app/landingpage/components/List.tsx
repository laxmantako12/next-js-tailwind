"use client";
import React, { useState, useEffect } from "react";
import {
  Card
} from "@/components/ui/card";

const List = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((data) => setTodos(data))
      .catch((error) => console.error("Error fetching data:", error));
  }, []);
  return (
    <div className="container py-14">
      <ul className="flex flex-wrap">
        {todos.map((todo) => (
          <li className="card p-2.5 basis-1/4" key={todo.id}>
            <Card className=" p-2.5">
              <h3 className="capitalize font-bold text-[18px] mb-1 text-primary/80">
                User: {todo.id}
              </h3>
              <p className="text-sm mb-2.5 text-primary/60">{todo.title}</p>
              <p className="text-sm text-primary/40">
                user Id:{todo.userId}
              </p>
            </Card>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default List;
