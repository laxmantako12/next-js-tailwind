"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Input } from "@/components/ui/input";
import { Card } from "@/components/ui/card";
import { Loader } from "lucide-react";

const Search = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [minAge, setMinAge] = useState("");
  const [maxAge, setMaxAge] = useState("");
  const [minSalary, setMinSalary] = useState("");
  const [maxSalary, setMaxSalary] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://dummy.restapiexample.com/api/v1/employees"
        );
        console.log("API response:", response.data); // Log the response data
        // Ensure response data is an array
        if (response.data && Array.isArray(response.data.data)) {
          setSearchResults(response.data.data);
          setError(""); // Clear any previous errors
        } else {
          setError("Unexpected response data format.");
        }
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error); // Log the error
        setError("Error fetching data. Please try again.");
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleSearch = () => {
    const query = searchQuery.toLowerCase();
    const filteredResults = searchResults.filter((result) => {
      const matchesName = result.employee_name.toLowerCase().includes(query);
      const matchesAge =
        (!minAge || result.employee_age >= minAge) &&
        (!maxAge || result.employee_age <= maxAge);
      const matchesSalary =
        (!minSalary || result.employee_salary >= minSalary) &&
        (!maxSalary || result.employee_salary <= maxSalary);
      return matchesName && matchesAge && matchesSalary;
    });
    setSearchResults(filteredResults);
  };

  return (
    <div className="container p-10">
      <h3 className="text-primary/80 mb-5">Greetings from GeeksforGeeks!</h3>
      <div>
        <Input
          type="text"
          placeholder="Search by name..."
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <div className="flex space-x-2 mt-2">
          <Input
            type="number"
            placeholder="Min Age"
            value={minAge}
            onChange={(e) => setMinAge(e.target.value)}
          />
          <Input
            type="number"
            placeholder="Max Age"
            value={maxAge}
            onChange={(e) => setMaxAge(e.target.value)}
          />
        </div>
        <div className="flex space-x-2 mt-2">
          <Input
            type="number"
            placeholder="Min Salary"
            value={minSalary}
            onChange={(e) => setMinSalary(e.target.value)}
          />
          <Input
            type="number"
            placeholder="Max Salary"
            value={maxSalary}
            onChange={(e) => setMaxSalary(e.target.value)}
          />
        </div>
        <button
          className="mt-4 bg-primary text-white p-2 rounded"
          onClick={handleSearch}
        >
          Search
        </button>
        {loading ? (
          <p>
            Loading... <Loader className="animate-spin" />
          </p>
        ) : (
          <ul className="flex flex-wrap mt-5">
            {searchResults.map((result) => (
              <li className="card p-2.5 basis-1/4" key={result.id}>
                <Card className="p-2.5">
                  <h3 className="capitalize font-bold text-[18px] mb-1 text-primary/80">
                    <b className="text-primary/80 mr-2">Name:</b>{" "}
                    {result.employee_name}
                  </h3>
                  <p className="text-sm mb-2.5 text-primary/60">
                    <b className="text-primary/80 mr-2">Salary:</b>
                    {result.employee_salary}
                  </p>
                  <p className="text-sm text-primary/40">
                    <b className="text-primary/80 mr-2">Age:</b>
                    {result.employee_age}
                  </p>
                </Card>
              </li>
            ))}
          </ul>
        )}
        {error && !loading && <p>{error}</p>}{" "}
        {/* Conditionally render the error message */}
      </div>
    </div>
  );
};

export default Search;
