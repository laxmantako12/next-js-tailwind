"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Input } from "@/components/ui/input";
import { Card } from "@/components/ui/card";

const Search = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://dummy.restapiexample.com/api/v1/employees"
        );
        console.log("API response:", response.data); // Log the response data
        // Ensure response data is an array
        if (response.data && Array.isArray(response.data.data)) {
          setSearchResults(response.data.data);
          setError(""); // Clear any previous errors
        } else {
          setError("Unexpected response data format.");
        }
        setLoading(false);
      } catch (error) {
        console.error("Error fetching data:", error); // Log the error
        setError("Error fetching data. Please try again.");
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  const handleSearch = (e) => {
    const query = e.target.value;
    setSearchQuery(query);
    const filteredResults = searchResults.filter((result) =>
      result.employee_name.toLowerCase().includes(query.toLowerCase())
    );
    setSearchResults(filteredResults);
  };

  return (
    <div className="container p-10">
      <h3 className="text-primary/80 mb-5">Greetings from GeeksforGeeks!</h3>
      <div>
        <Input
          type="text"
          placeholder="Search..."
          value={searchQuery}
          onChange={handleSearch}
        />
        {loading ? (
          <p>Loading...</p>
        ) : (
          <ul className="flex flex-wrap mt-5">
            {searchResults.map((result) => (
              <li className="card p-2.5 basis-1/4" key={result.id}>
                <Card className="p-2.5">
                  <h3 className="capitalize font-bold text-[18px] mb-1 text-primary/80">
                    <b className="text-primary/80 mr-2">Name:</b>{" "}
                    {result.employee_name}
                  </h3>
                  <p className="text-sm mb-2.5 text-primary/60">
                    <b className="text-primary/80 mr-2">Salary:</b>
                    {result.employee_salary}
                  </p>
                  <p className="text-sm text-primary/40">
                    <b className="text-primary/80 mr-2">Age:</b>
                    {result.employee_age}
                  </p>
                </Card>
              </li>
            ))}
          </ul>
        )}
        {error && !loading && <p>{error}</p>} {/* Conditionally render the error message */}
      </div>
    </div>
  );
};

export default Search;
